all: audio video receiver receiverllg

CC = gcc
CFLAGS = -Wall -Wextra
LDLIBS = -lcrypto

audio: aes_decrypt.c audio.c
	@grep 'const uint8_t key' aes_decrypt.c | shasum | grep b063ea5cf637e303a770c6674d49eab858bca288 > /dev/null && echo Did you insert the real key into aes_decrypt.c\? || echo good. aes_decrypt.c has a non-default key
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

video: aes_decrypt.c video.c
	@grep 'const uint8_t key' aes_decrypt.c | shasum | grep b063ea5cf637e303a770c6674d49eab858bca288 > /dev/null && echo Did you insert the real key into aes_decrypt.c\? || echo good. aes_decrypt.c has a non-default key
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

receiver: aes_decrypt.c receiver.c
	@grep 'const uint8_t key' aes_decrypt.c | shasum | grep b063ea5cf637e303a770c6674d49eab858bca288 > /dev/null && echo Did you insert the real key into aes_decrypt.c\? || echo good. aes_decrypt.c has a non-default key
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)
	sudo setcap cap_net_raw+ep $@

receiverllg: aes_decrypt.c receiver_LunaLovegood.c
	@grep 'const uint8_t key' aes_decrypt.c | shasum | grep b063ea5cf637e303a770c6674d49eab858bca288 > /dev/null && echo Did you insert the real key into aes_decrypt.c\? || echo good. aes_decrypt.c has a non-default key
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)
	sudo setcap cap_net_raw+ep $@
